<?php

class BooksAPI
{

	public function formatData($books)
	{
		if (!empty($books)) {
			$return = array();
			foreach($books as $book) {
			 	$_book = array(
			 		'ISBN' => $book->isbn,
			 		'author' => $book->author,
			 		'title'=> $book->title,
			 		'published' => $book->published_date,
			 		'rating' => $book->rating
			 	);
			 	array_push($return, $_book);
			}
		} else {
			$return = array(
				'error' => true,
				'message' => 'We do not have any books listed under this criteria'
			);
		}
		return $return;
	}

	public function getByISBN($isbn)
	{
		$books = Book::where('isbn', '=', $isbn)->get();
		return $books;
	}

	public function getByAuthor($author)
	{
		$books = Book::where('author', '=', $author)->get();
		return $books;
	}

	public function getByTitle($title)
	{
		$books = Book::where('title', '=', $title)->get();
		return $books;
	}

	public function getByDateRange($from, $to)
	{
		$from = new DateTime($from);
		$to = new DateTime($to);
		$fromTS = date('Y-m-d H:i:s', $from->getTimestamp());
		$toTS = date('Y-m-d H:i:s', $to->getTimestamp());
		$books = Book::where('published_date', '>=', $fromTS)->where('published_date', '<=', $toTS)->get();
		return $books;
	}

	public function getByRating($rating)
	{
		$books = Book::where('rating', '>=', $rating)->get();
		return $books;
	}

}