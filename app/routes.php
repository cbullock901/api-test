<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

// Public
Route::get('/', array('as' => 'home', 'uses' => 'PublicController@homePage'));
Route::get('/search', array('uses' => 'PublicController@search'));
Route::post('/search', array('uses' => 'PublicController@searchResults'));

Route::get('api/getbooksbyauthor/{authorname}', array('uses' => 'BooksController@getBooksByAuthor'));
Route::get('api/getbookbyisbn/{isbn}', array('uses' => 'BooksController@getBookByISBN'));
Route::get('api/getbooksbytitle/{title}', array('uses' => 'BooksController@getBooksByTitle'));
Route::get('api/getbooksbyrating/{rating}', array('uses' => 'BooksController@getBooksByRating'));
Route::get('api/getbooksbydaterange/{from}/{to}', array('uses' => 'BooksController@getBooksByDateRange'));