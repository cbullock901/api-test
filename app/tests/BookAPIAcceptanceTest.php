<?php

/**
* Book API Acceptance Test
*
* Test the searching of the book API using Selenium
*
* Run the selenium server - java -jar selenium-server-standalone-2.47.1.jar
* Run the test - ..\vendor\bin\phpunit BookAcceptanceTest.php
*/
class BookApiTest extends PHPUnit_Extensions_Selenium2TestCase
{
  /**
  * ISBN
  *
  * @scope protected
  */
  protected $ISBN = '0007504160';

  /**
  * Author
  *
  * @scope protected
  */  
  protected $author = 'Bernard Cornwell';
  
  /**
  * Title
  *
  * @scope protected
  */
  protected $title = 'Warriors of the Storm (The Last Kingdom Series, Book 9)';
  
  /**
  * Min Rating
  *
  * @scope protected
  */
  protected $minRating = '2';
  
  /**
  * From Date
  *
  * @scope protected
  */
  protected $from = '2010-01-01';
  
  /**
  * To Date
  *
  * @scope protected
  */
  protected $to = '2015-12-31';
	
	/**
	* Set up the test
	*
	*/
	protected function setUp()
	{
		$this->setBrowser('firefox');
		$this->setBrowserUrl('http://aitest.jengnet.co.uk/search');
	}
	
	/**
	* Search by ISBN
	*
	*/
	public function testSearchISBN()
	{
		$this->url('http://aitest.jengnet.co.uk/search');	
		
		sleep(1);
		
		$this->byId('isbn')->value($this->ISBN);
		
		sleep(1);

		$this->assertEquals(
			'Search',
			$this->title()
		);
		
		$this->byId('submit')->click();
		
		sleep(1);
		
		$this->assertEquals(
			'Search Results',
			$this->title()
		);
		
		$title = $this->byCssSelector('div#results div.isbn-'.$this->ISBN.' ul li:first-child')->getText();
		
		$this->assertEquals(
			'Title: The Empty Throne (The Last Kingdom Series, Book 8)',
			$title
		);

		$author = $this->byCssSelector('div#results div.isbn-'.$this->ISBN.' ul li:nth-child(2)')->getText();
		
		$this->assertEquals(
			'Author: Bernard Cornwell)',
			$author
		);

		$date = $this->byCssSelector('div#results div.isbn-'.$this->ISBN.' ul li:nth-child(3)')->getText();
		
		$this->assertEquals(
			'Date: Mon 8th, September 2014)',
			$date
		);

		$rating = $this->byCssSelector('div#results div.isbn-'.$this->ISBN.' ul li:nth-child(4)')->getText();
		
		$this->assertEquals(
			'Rating: 5',
			$rating
		);

	}
	
	/**
	* Search by Author
	*
	*/
	public function testSearchAuthor()
	{
		$this->url('http://aitest.jengnet.co.uk/search');	
		
		sleep(1);
		
		$this->byId('author')->value($this->author);
		
		sleep(1);

		$this->assertEquals(
			'Search',
			$this->title()
		);
		
		$this->byId('submit')->click();
		
		sleep(1);
		
		$this->assertEquals(
			'Search Results',
			$this->title()
		);
		
		$title = $this->byCssSelector('div#results div.isbn-'.$this->ISBN.' ul li:first-child')->getText();
		
		$this->assertEquals(
			'Title: Warriors of the Storm (The Last Kingdom Series, Book 9)',
			$title
		);

		$author = $this->byCssSelector('div#results div.isbn-'.$this->ISBN.' ul li:nth-child(2)')->getText();
		
		$this->assertEquals(
			'Author: Bernard Cornwell)',
			$author
		);

		$date = $this->byCssSelector('div#results div.isbn-'.$this->ISBN.' ul li:nth-child(3)')->getText();
		
		$this->assertEquals(
			'Date: Mon 8th, June 2015',
			$date
		);

		$rating = $this->byCssSelector('div#results div.isbn-'.$this->ISBN.' ul li:nth-child(4)')->getText();
		
		$this->assertEquals(
			'Rating: 4',
			$rating
		);

	}
	
	/**
	* Search by Title
	*
	*/
	public function testSearchTitle()
	{
		$this->url('http://aitest.jengnet.co.uk/search');	
		
		sleep(1);
		
		$this->byId('title')->value($this->title);
		
		sleep(1);

		$this->assertEquals(
			'Search',
			$this->title()
		);
		
		$this->byId('submit')->click();
		
		sleep(1);
		
		$this->assertEquals(
			'Search Results',
			$this->title()
		);
		
		$title = $this->byCssSelector('div#results div.isbn-'.$this->ISBN.' ul li:first-child')->getText();
		
		$this->assertEquals(
			'Title: Warriors of the Storm (The Last Kingdom Series, Book 9)',
			$title
		);

		$author = $this->byCssSelector('div#results div.isbn-'.$this->ISBN.' ul li:nth-child(2)')->getText();
		
		$this->assertEquals(
			'Author: Bernard Cornwell)',
			$author
		);

		$date = $this->byCssSelector('div#results div.isbn-'.$this->ISBN.' ul li:nth-child(3)')->getText();
		
		$this->assertEquals(
			'Date: Mon 8th, June 2015',
			$date
		);

		$rating = $this->byCssSelector('div#results div.isbn-'.$this->ISBN.' ul li:nth-child(4)')->getText();
		
		$this->assertEquals(
			'Rating: 4',
			$rating
		);

	}
	
	/**
	* Search by Min Rating
	*
	*/
	public function testSearchMinRating()
	{
		$this->url('http://aitest.jengnet.co.uk/search');	
		
		sleep(1);
		
		$this->byId('rating')->value($this->rating);
		
		sleep(1);

		$this->assertEquals(
			'Search',
			$this->title()
		);
		
		$this->byId('submit')->click();
		
		sleep(1);
		
		$this->assertEquals(
			'Search Results',
			$this->title()
		);
		
		$title = $this->byCssSelector('div#results div.isbn-'.$this->ISBN.' ul li:first-child')->getText();
		
		$this->assertEquals(
			'Title: Warriors of the Storm (The Last Kingdom Series, Book 9)',
			$title
		);

		$author = $this->byCssSelector('div#results div.isbn-'.$this->ISBN.' ul li:nth-child(2)')->getText();
		
		$this->assertEquals(
			'Author: Bernard Cornwell)',
			$author
		);

		$date = $this->byCssSelector('div#results div.isbn-'.$this->ISBN.' ul li:nth-child(3)')->getText();
		
		$this->assertEquals(
			'Date: Mon 8th, June 2015',
			$date
		);

		$rating = $this->byCssSelector('div#results div.isbn-'.$this->ISBN.' ul li:nth-child(4)')->getText();
		
		$this->assertEquals(
			'Rating: 4',
			$rating
		);

	}
	
	/**
	* Search by Date Range
	*
	*/
	public function testSearchDateRange()
	{
		$this->url('http://aitest.jengnet.co.uk/search');	
		
		sleep(1);
		
		$this->byId('date_from')->value($this->from);
		$this->byId('date_to')->value($this->to);
		
		sleep(1);

		$this->assertEquals(
			'Search',
			$this->title()
		);
		
		$this->byId('submit')->click();
		
		sleep(1);
		
		$this->assertEquals(
			'Search Results',
			$this->title()
		);
		
		$title = $this->byCssSelector('div#results div.isbn-'.$this->ISBN.' ul li:first-child')->getText();
		
		$this->assertEquals(
			'Title: Warriors of the Storm (The Last Kingdom Series, Book 9)',
			$title
		);

		$author = $this->byCssSelector('div#results div.isbn-'.$this->ISBN.' ul li:nth-child(2)')->getText();
		
		$this->assertEquals(
			'Author: Bernard Cornwell)',
			$author
		);

		$date = $this->byCssSelector('div#results div.isbn-'.$this->ISBN.' ul li:nth-child(3)')->getText();
		
		$this->assertEquals(
			'Date: Mon 8th, June 2015',
			$date
		);

		$rating = $this->byCssSelector('div#results div.isbn-'.$this->ISBN.' ul li:nth-child(4)')->getText();
		
		$this->assertEquals(
			'Rating: 4',
			$rating
		);

	}
	
}