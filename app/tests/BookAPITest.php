<?php

class BookAPITest extends TestCase {

  /**
  * API
  *
  * @scope protected
  */
	protected $api;

  /**
  * ISBN
  *
  * @scope protected
  */
  protected $ISBN = '0007504160';

  /**
  * Author
  *
  * @scope protected
  */  
  protected $author = 'Bernard Cornwell';
  
  /**
  * Title
  *
  * @scope protected
  */
  protected $title = 'Warriors of the Storm (The Last Kingdom Series, Book 9)';
  
  /**
  * Min Rating
  *
  * @scope protected
  */
  protected $minRating = '2';
  
  /**
  * From Date
  *
  * @scope protected
  */
  protected $from = '2010-01-01';
  
  /**
  * To Date
  *
  * @scope protected
  */
  protected $to = '2015-12-31';

	protected function setUp()
	{
		$this->api = new BooksAPI();
	}

	/**
	 * Test get book by ISBN
	 *
	 * @return void
	 */
	public function testgetByISBN()
	{
		$books = $this->api->getByISBN($this->ISBN);
		if (!empty($books) && is_array($books)) {
			$this->assertInstanceOf('Book', $books[0]);
			$this->assertEquals(
				$this->ISBN,
				$books[0]->isbn
			);
			$this->assertEquals(
				$this->title,
				$books[0]->title
			);
			$this->assertEquals(
				$this->author,
				$books[0]->author
			);
			$this->assertEquals(
				$this->rating,
				$books[0]->rating
			);
		}
	}

	/**
	 * Test get book by Author
	 *
	 * @return void
	 */
	public function testgetByAuthor()
	{
		$books = $this->api->getByAuthor($this->author);
		if (!empty($books) && is_array($books)) {
			$this->assertInstanceOf('Book', $books[0]);
		}
	}

	/**
	 * Test get book by Author
	 *
	 * @return void
	 */
	public function testgetByTitle()
	{
		$books = $this->api->getBytitle($this->title);
		if (!empty($books) && is_array($books)) {
			$this->assertInstanceOf('Book', $books[0]);
		}
	}

	/**
	 * Test get books by min rating
	 *
	 * @return void
	 */
	public function testgetByRating()
	{
		$books = $this->api->getByRating($this->rating);
		if (!empty($books) && is_array($books)) {
			$this->assertInstanceOf('Book', $books[0]);
		}
	}

	/**
	 * Test get books by min rating
	 *
	 * @return void
	 */
	public function testgetByRating()
	{
		$books = $this->api->getByDateRange($this->from, $this->to);
		if (!empty($books) && is_array($books)) {
			$this->assertInstanceOf('Book', $books[0]);
		}
	}

}