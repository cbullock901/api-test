<!doctype html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>Search</title>
  <style>
      @import url(//fonts.googleapis.com/css?family=Lato:300,400,700);

      body {
          margin:0;
          font-family:'Lato', sans-serif;
          color: #999;
      }
      form {
        width:400px;
        margin:0 auto;
        padding:20px;
        background:#f1f1f1;
      }
      form div.row {
        padding:0 0 20px 0;
        overflow:hidden;
      }
      form div.row label {
        float:left;
        width:150px;
      }
      div.alert {
        width:400px;
        padding:20px;
        margin:0 auto 20px auto;
        color: #a94442;
        background-color: #f2dede;
        border-color: #ebccd1;
      }
      div.alert h4 {
        margin:0;
        padding:0;
        font-weight:normal;
      }
  </style>
</head>
<body>
  @if(Session::get('error'))
  <div class="alert">
    <h4>{{ Session::get('error') }}</h4>
  </div>
  @endif
  <form method="post">
    <h1>Search</h1>
    <div class="row">
      <label>ISBN</label>
      <input type="text" name="isbn" id="isbn">
    </div>
    <div class="row">
      <label>Author</label>
      <input type="text" name="author" id="author">
    </div>
    <div class="row">
      <label>Title</label>
      <input type="text" name="title" id="title">
    </div>
    <div class="row">
      <label>Min Rating</label>
      <input type="text" name="rating" id="rating">
    </div>
    <div class="row">
      <label>Date From</label>
      <input type="text" name="date_from" id="date_from">
    </div>
    <div class="row">
      <label>Date To</label>
      <input type="text" name="date_to">
    </div>
    <input type="submit" name="submit" id="submit" value="Search">  
  </form>
</body>
</html>
