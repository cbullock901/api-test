<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Search Results</title>
    <style>
        @import url(//fonts.googleapis.com/css?family=Lato:300,400,700);

        body {
            margin:0;
            font-family:'Lato', sans-serif;
            color: #999;
        }
    </style>
</head>
<body>
    <h1>Search Results</h1>
    @if (!empty($books))
      <div id="results">
      @foreach($books as $book)
        <div class="book isbn-{{$book->ISBN}}">
          <ul>
            <li>Title: {{$book->title}}</li>
            <li>Author: {{$book->author}}</li>
            <li>Date: {{date('D jS, F Y', strtotime($book->published))}}</li>
            <li>Rating: {{$book->rating}}</li>
            <li>ISBN: {{$book->ISBN}}</li>
          </ul>
        </div>
      @endforeach
      </div>
      <P><a href="/search">Back to search</a></p>
    @else
    <p>We do not have any books which match your criteria, please <a href="/search">Search again</a></p>
    @endif
</body>
</html>
