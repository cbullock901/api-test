<?php

class Book extends Eloquent
{

	protected $table = 'books';

	protected $fillable = array(
		'isbn',
		'author',
		'title',
		'published_date',
		'rating'
	);

}