<?php

/*
The aim of this task is to test knowledge of API centric architecture. The task should not take too long
 
Our newest projects are built in "Zend Framework 2" for this reason the task ideally should be done using ZF2.
If the candidate not familiar with ZF2 he is free to use any other technology.
 
Task:
 
Build a web service providing information about books.
It should be possible to ask that service for books by a given ISBN, author, title, between a given date range or by minimum rating.
We expect a good entity/model design where data entities and business logic are encapsulated.
We will be looking for design decisions that will allow the solution to be technology agnostic, modular, following OOP principles, for a solution that is tested against error conditions and bad returns.
By technology agnostic we mean that without a major refactor it should be possible to introduce another transport layer (SOAP/XML/REST), storage layer or swap out any other of the components.
Your code should have functional tests as well as unit tests.
Feel free to use any readily available components, but remember that it's your code that will ultimately be checked.
We are expecting a PHP5.5 compatible code, backwards compatibility is not required.
We are keen to see fresh thinking, clean, easy to follow, modern code.
We really like to read lorem ipsums - please don't spend time looking for real life data.
Please provide instructions how to run and test.
*/


class BooksController extends BaseController
{

	protected $api;

	public function __construct()
	{
		$this->api = new BooksAPI();
	}

	public function getBooksByAuthor($author)
	{
		$books = $this->api->getByAuthor($author);
		echo json_encode($this->api->formatData($books));
	}

	public function getBooksByTitle($title)
	{
		$books = $this->api->getBytitle($title);
		echo json_encode($this->api->formatData($books));
	}

	public function getBookByISBN($isbn)
	{
		$books = $this->api->getByISBN($isbn);
		echo json_encode($this->api->formatData($books));
	}

	public function getBooksByRating($rating)
	{
		$books = $this->api->getByRating($rating);
		echo json_encode($this->api->formatData($books));
	}

	public function getBooksByDateRange($from, $to)
	{
		$books = $this->api->getByDateRange($from, $to);
		echo json_encode($this->api->formatData($books));
	}

}