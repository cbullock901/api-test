<?php

/*
The aim of this task is to test knowledge of API centric architecture. The task should not take too long
 
Our newest projects are built in "Zend Framework 2" for this reason the task ideally should be done using ZF2.
If the candidate not familiar with ZF2 he is free to use any other technology.
 
Task:
 
Build a web service providing information about books.
It should be possible to ask that service for books by a given ISBN, author, title, between a given date range or by minimum rating.
We expect a good entity/model design where data entities and business logic are encapsulated.
We will be looking for design decisions that will allow the solution to be technology agnostic, modular, following OOP principles, for a solution that is tested against error conditions and bad returns.
By technology agnostic we mean that without a major refactor it should be possible to introduce another transport layer (SOAP/XML/REST), storage layer or swap out any other of the components.
Your code should have functional tests as well as unit tests.
Feel free to use any readily available components, but remember that it's your code that will ultimately be checked.
We are expecting a PHP5.5 compatible code, backwards compatibility is not required.
We are keen to see fresh thinking, clean, easy to follow, modern code.
We really like to read lorem ipsums - please don't spend time looking for real life data.
Please provide instructions how to run and test.
*/


class PublicController extends BaseController
{

	public function __construct()
	{

	}

	public function homePage()
	{

	}

	public function search()
	{
		return View::make('search')->with('headerClass', 'darkerShadow');
	}

	public function searchResults()
	{
		if (Input::get('isbn')) {
			$url = Request::root().'/api/getbookbyisbn/'.Input::get('isbn');
		} elseif (Input::get('author')) {
			$url = Request::root().'/api/getbooksbyauthor/'.rawurlencode(Input::get('author'));
		} elseif (Input::get('title')) {
			$url = Request::root().'/api/getbooksbytitle/'.rawurlencode(Input::get('title'));
		} elseif (Input::get('rating')) {
			$url = Request::root().'/api/getbooksbyrating/'.Input::get('rating');
		} elseif (Input::get('date_from')) {
			$url = Request::root().'/api/getbooksbydaterange/'.Input::get('date_from').'/'.Input::get('date_to');
		}

		if (isset($url)) {
			$json = file_get_contents($url);
			$books = json_decode($json);
			return View::make('results')->with('books', $books);
		} else {
			Session::flash('error', 'Please add search criteria');
			return Redirect::to('search');
		}
	}

}