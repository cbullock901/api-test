## AI API Test

This is an API based on a RESTful web service. Each API returns JSON

### Public search UI

http://aitest.jengnet.co.uk/search

### Books MySQL API Lib

/app/library/BooksAPI.php

### Books Model

/app/models/Book.php

### Books API Controller

/app/controllers/BooksController.php

## API's

### Get Books by author

http://aitest.jengnet.co.uk/api/getbooksbyauthor/Bernard%20Cornwell

### Get book by ISBN

http://aitest.jengnet.co.uk/api/getbookbyisbn/0007504071

### Get books by date range

http://aitest.jengnet.co.uk/api/getbooksbydaterange/2010-01-01/2015-12-31

### Get books by min rating

http://aitest.jengnet.co.uk/api/getbooksbyrating/1

## Tests

### Acceptance tests

/app/tests/BookAPIAcceptanceTest.php

### Unit Tests

/app/tests/BookAPITest.php
